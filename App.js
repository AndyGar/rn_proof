import React, { Component } from "react";
import { Text, View, Button, Image, StyleSheet, Animated, Easing } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import { FluidNavigator, Transition } from "react-navigation-fluid-transitions";
import fromTop from "./src/transitions/fromTop";

const LogoImage = props => (
  <Image
    source={{ uri: "https://picsum.photos/100/100?image=56" }}
    style={props.style}
  />
);

class Screen1 extends React.Component {
  render() {
    return (
        <View style={[styles.container, { backgroundColor: "#7FDBFF" }]}>
          <LogoImage style={styles.largeLogo} />
        <Text style={styles.paragraph}>Esta es una prueba de concepto de transiciones</Text>
        <Text style={styles.paragraph}>La primera es una transicion horizontal</Text>
          <Button
            title="Transicion 1 (horizontal)"
            onPress={() => this.props.navigation.navigate("screen2")}
          />
        </View>
    );
  }
}

class Screen2 extends React.Component {
  render() {
    return (
      <View style={[styles.container, { backgroundColor: "#FFDC00" }]}>
        <Text style={styles.paragraph}>
            La siguiente es una transicion vertical
        </Text>
        <Button title="Atras (horizontal)" onPress={() => this.props.navigation.goBack()} />
        <Button title="Transicion 2 (vertical)" onPress={() => this.props.navigation.navigate("screen3")} />
        </View>
    );
  }
}

class Screen3 extends React.Component {
  render() {
    return (
      <View style={[styles.container, { backgroundColor: "#39CCCC" }]}>
        <Text style={styles.paragraph}>
          La siguiente es una transicion de zoom
        </Text>
          <Button title="Atras (vertical)" onPress={() => this.props.navigation.goBack()} />
          <Button title="Transicion 3 (zoom)" onPress={() => this.props.navigation.navigate("screen4")} />
        </View>
    );
  }
}

class Screen4 extends React.Component {
  render() {
    return (
        <View style={[styles.container, { backgroundColor: "#2ECC40" }]}>
        <Text style={styles.paragraph}>
            En las dos siguientes transiciones podras ver transiciones de elementos, no de pantallas. Moviendo la imagen de forma suave y el texto desplazandolo lateralmente
        </Text>
          <Button title="Atras (zoom)" onPress={() => this.props.navigation.goBack()} />
          <Button title="Transicion de logo" onPress={() => this.props.navigation.navigate("screen5")} />
        </View>
    );
  }
}

class Screen5 extends React.Component {
  render() {
    return (
        <View style={styles.container}>
        <Transition shared="logo">
          <LogoImage style={styles.largeLogo} />
        </Transition>
        <Transition appear="horizontal">
        <Text style={styles.paragraph}>
            Dando a siguiente podras ver una transicion del logo y del texto
        </Text>
        </Transition>
        <Button title="Atras (zoom)" onPress={() => this.props.navigation.goBack()} />
        <Button title="Siguiente" onPress={() => this.props.navigation.navigate("screen6")} />
        </View>
    );
  }
}


class Screen6 extends React.Component {
  render() {
    return (
        <View style={styles.container}>
        <Transition shared="logo">
          <LogoImage style={styles.smallLogo} />
        </Transition>
        <Transition appear="horizontal">
        <Text style={styles.paragraph}>
          Esta es la ultima pantalla, ahora puedes ir hacia atras viendo las transiciones "del reves"
        </Text>
        </Transition>
        <Button title="Atras (logo)" onPress={() => this.props.navigation.goBack()} />
        </View>
    );
  }
}

const transitionConfig = () => {
  return {
    transitionSpec: {
      duration: 3000,
      easing: Easing.out(Easing.poly(4)),
      timing: Animated.timing,
      useNativeDriver: true,
    },
    screenInterpolator: ({ layout, position, scene }) => {
      const { index } = scene;
      const { initHeight } = layout;

      const translateY = position.interpolate({
        inputRange: [index - 1, index, index + 1],
        outputRange: [-initHeight, 0, 0],
      });

      const opacity = position.interpolate({
          inputRange: [index - 1, index - 0.99, index],
          outputRange: [0, 1, 1],
        });

      return { opacity, transform: [{ translateY }] };
    },
  };

}


const navigator = createStackNavigator({
  screen1: { screen: Screen1 },
  screen2: { screen: Screen2 },
  screen3: { screen: Screen3 },
  screen4: { screen: Screen4 },
  screen5: { screen: Screen5 },
  screen6: { screen: Screen6 }
}, {
  initialRouteName: 'screen1',
  transitionConfig,
  });

const AppContainer = createAppContainer(navigator);

// const MyTransitionSpec = ({
//   duration: 2000,
//   easing: Easing.bezier(0.2833, 0.99, 0.31833, 0.99),
//   timing: Animated.timing,
// });

// const transitionConfig = () => {
//   return {
//     transitionSpec: {
//       duration: 750,
//       easing: Easing.out(Easing.poly(4)),
//       timing: Animated.timing,
//       useNativeDriver: true,
//     },
//     screenInterpolator: sceneProps => {      
//       const { layout, position, scene } = sceneProps

//       const thisSceneIndex = scene.index
//       const width = layout.initWidth

//       const translateX = position.interpolate({
//         inputRange: [thisSceneIndex - 1, thisSceneIndex],
//         outputRange: [width, 0],
//       })

//       return { transform: [ { translateX } ] }
//     },
//   }
// }

console.disableYellowBox = true;

export default class App extends Component {
  render() {
    return <AppContainer />;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around",
    backgroundColor: "#ecf0f1"
  },
  largeLogo: {
    width: 200,
    height: 200,
    borderRadius: 100
  },
  smallLogo: {
    width: 80,
    height: 80,
    borderRadius: 40
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: "bold",
    textAlign: "center",
    color: "#34495e"
  }
});